
# Deploy a Web App on EC2 (Manually)
In this project we will deploy a web application on an EC2 instance manually.

## Technologies Used
- AWS
- Docker
- Linux

## Project Description
- Create and configure an EC2 Instance on AWS
- Install Docker on remote EC2 Instance
- Deploy Docker image from private Docker repository on EC2 Instance

## Prerequisites
- An AWS account
- Docker Account
- Linux machine configured with Docker and Git

## Guide Steps

### Create EC2 Instance
- Log in to the AWS Portal
- Compute > EC2 > Launch Instance
	- We don't have any templates so we'll launch a blank one
- Creation Page Configuration
	- Name: **my-instance**
		- Tag: **Type**: **web-server-with-docker**
	- App & OS Images: **Amazon Linux**
	- Instance Type: **t2.micro**
		- This is part of the free tier and should be sufficient for Docker and a mini web server
	- Key Pair
		- Create Key Pair
		- Name: **docker-server**
		- Type: **RSA**
		- File: **.pem**
			- If you are on Windows, use instead the .ppk option
	- Network Settings
		- Default VPC selected
		- Subnet: **No Preference**
		- Auto-assign Public IP: **Enabled**
		- Create Security Group
			- Name: **security-group-docker-server**
		- Inbound Security Rules
			- Type: **SSH**
			- Source Type: **My IP**
	- Configure Storage
		- Default (8 GiB)
	- **Launch Instance**

### Connect via SSH
- Move the **.pem** we downloaded to our home directories **.ssh** folder
	- `mv ~/Downloads\docker-server.pem ~/.ssh`
- Modify file permissions to be more restrictive
	- `chmod 400 ~/.ssh/docker-server.pem`
- SSH into our new EC2 instance
	- `ssh -i ~/.ssh/docker-server.pem ec2-user@EC2_PUBLIC_IP`
		- Obtained the **EC2_PUBLIC_IP** for the instance dashboard
		- **ec2-user** is the default user

![Successful SSH](/images/m9-1-successful-ssh-to-ec2.png)

### Install Docker on EC2
- Update Yum
	- `sudo yum update`
- Install Docker
	- `sudo yum install docker`
- Start Docker
	- `sudo service docker start`
- Add user as member of docker group (prevents needing **sudo** to run docker)
	- `sudo usermod -aG docker $USER`
	- Perform a log off/ log in for the changes to take effect
		- `exit`
		- `ssh -i ~/.ssh/docker-server.pem ec2-user@EC2_PUBLIC_IP`
- Test everything works as intended
	- `docker run redis`
		- This command should download **Redis** and run a container from the image without **sudo** needed

### Build and Upload Docker Image
**On Local Machine**

- Download project and navigate to folder
	- `git clone https://github.com/techworld-with-nana/react-nodejs-example`
	- `cd react-nodejs-example`
- Build The Image
	- `docker build -t app .`
- Tag Image
	- `docker tag app:latest DOCKER_REPO:1.0`
		- The DOCKER_REPO is your private Docker repository username/app-name
- Upload Image to Private Repo
	- `docker login`
	- `docker push DOCKER_REPO:1.0`

![Successful Image Upload](/images/m9-1-successful-docker-image-upload.png)

### Run Docker Container from Private Repo

**On EC2 Server**

- `docker login`
- `docker pull DOCKER_REPO:1.0`
- `docker run -d -p 3000:3080 DOCKER_REPO:1.0`
	- `-d` for detached mode
	- `-p` to bind our container port to our EC2 port
		- 3080 is used for our web app, found in the **Dockerfile**

![Successful Docker Run](/images/m9-1-successful-docker-run.png)

### Configure EC2 Firewall for External Access
- AWS Console > Firewall > Inbound rules > Edit Inbound Rules
- Add Rule
	- Type: **Custom TCP**
	- Port Range: **3000**
	- Source: **Custom: My IP**
- Browser: **EC2_PUBLIC_IP:3000**

![App Accessible](/images/m9-1-app-accessible.png)

![App Functional](/images/m9-1-app-functional.png) 
